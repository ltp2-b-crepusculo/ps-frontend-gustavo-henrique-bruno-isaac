import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import SignUpView from '../views/SignUpView.vue'
import DoctorDashboard from '../views/DoctorDashboard.vue'
import PatientDashboard from '../views/PatientDashboard.vue'
import AdminDashboard from '../views/AdminDashboard.vue'
import ProfileView from '../views/ProfileView.vue'
import ScheduleAppointment from '../views/ScheduleAppointment.vue'
import PatientAppointments from '../views/PatientAppointments.vue'
import DoctorAppointments from '../views/DoctorAppointments.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  {
    path: '/signup',
    name: 'signup',
    component: SignUpView
  },
  {
    path: '/doctor-dashboard',
    name: 'doctorDashboard',
    component: DoctorDashboard,
    meta: { requiresAuth: true, role: 'doctor' }
  },
  {
    path: '/patient-dashboard',
    name: 'patientDashboard',
    component: PatientDashboard,
    meta: { requiresAuth: true, role: 'patient' }
  },
  {
    path: '/admin-dashboard',
    name: 'adminDashboard',
    component: AdminDashboard,
    meta: { requiresAuth: true, role: 'admin' }
  },
  {
    path: '/profile',
    name: 'profile',
    component: ProfileView,
    meta: { requiresAuth: true }
  },
  {
    path: '/scheduleAppointment',
    name: 'scheduleAppointment',
    component: ScheduleAppointment,
    meta: { requiresAuth: true, role: 'patient' }
  },
  {
    path: '/patientAppointments',
    name: 'patientAppointments',
    component: PatientAppointments,
    meta: { requiresAuth: true, role: 'patient' }
  },
  {
    path: '/doctorAppointments',
    name: 'doctorAppointments',
    component: DoctorAppointments,
    meta: { requiresAuth: true, role: 'doctor' }
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  const token = localStorage.getItem('token')
  const role = localStorage.getItem('role')

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!token) {
      next({ name: 'login' })
    } else {
      if (to.matched.some(record => record.meta.role)) {
        const requiredRole = to.meta.role
        if (requiredRole === role) {
          next()
        } else {
          next({ name: 'home' })
        }
      } else {
        next()
      }
    }
  } else {
    next()
  }
})

export default router
